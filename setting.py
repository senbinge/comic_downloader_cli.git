# -*- coding: utf-8 -*-
"""爬虫配置文件"""
# import os
# import sys
#
# # MYSQL
# MYSQL_IP = "localhost"
# MYSQL_PORT = 3306
# MYSQL_DB = ""
# MYSQL_USER_NAME = ""
# MYSQL_USER_PASS = ""
#
# # MONGODB
# MONGO_IP = "localhost"
# MONGO_PORT = 27017
# MONGO_DB = ""
# MONGO_USER_NAME = ""
# MONGO_USER_PASS = ""
#
# # REDIS
# # ip:port 多个可写为列表或者逗号隔开 如 ip1:port1,ip2:port2 或 ["ip1:port1", "ip2:port2"]
# REDISDB_IP_PORTS = "localhost:6379"
# REDISDB_USER_PASS = ""
# REDISDB_DB = 0
# # 适用于redis哨兵模式
# REDISDB_SERVICE_NAME = ""
#
# # 数据入库的pipeline，可自定义，默认MysqlPipeline
# ITEM_PIPELINES = [
#     "feapder.pipelines.mysql_pipeline.MysqlPipeline",
#     # "feapder.pipelines.mongo_pipeline.MongoPipeline",
# ]
# EXPORT_DATA_MAX_FAILED_TIMES = 10 # 导出数据时最大的失败次数，包括保存和更新，超过这个次数报警
# EXPORT_DATA_MAX_RETRY_TIMES = 10 # 导出数据时最大的重试次数，包括保存和更新，超过这个次数则放弃重试
#
# # 爬虫相关
# # COLLECTOR
# COLLECTOR_SLEEP_TIME = 1  # 从任务队列中获取任务到内存队列的间隔
# COLLECTOR_TASK_COUNT = 10  # 每次获取任务数量
#
# # SPIDER
#加快下载速度
SPIDER_THREAD_COUNT =3  # 爬虫并发数
# SPIDER_SLEEP_TIME = 0  # 下载时间间隔 单位秒。 支持随机 如 SPIDER_SLEEP_TIME = [2, 5] 则间隔为 2~5秒之间的随机数，包含2和5
# SPIDER_TASK_COUNT = 1  # 每个parser从内存队列中获取任务的数量
# SPIDER_MAX_RETRY_TIMES = 100  # 每个请求最大重试次数
# KEEP_ALIVE = False  # 爬虫是否常驻
#


# # 设置代理
# PROXY_EXTRACT_API = None  # 代理提取API ，返回的代理分割符为\r\n
# PROXY_ENABLE = True
#
# # 随机headers
RANDOM_HEADERS = True
# # UserAgent类型 支持 'chrome', 'opera', 'firefox', 'internetexplorer', 'safari'，'mobile' 若不指定则随机类型
# USER_AGENT_TYPE = "chrome"
# # 默认使用的浏览器头 RANDOM_HEADERS=True时不生效
# DEFAULT_USERAGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"
# # requests 使用session
# USE_SESSION = False

# LOG_NAME = os.path.basename(os.getcwd())
# LOG_PATH = "log/%s.log" % LOG_NAME  # log存储路径
LOG_LEVEL = "INFO"
# LOG_COLOR = True  # 是否带有颜色
# LOG_IS_WRITE_TO_CONSOLE = True # 是否打印到控制台
# LOG_IS_WRITE_TO_FILE = False  # 是否写文件
# LOG_MODE = "w"  # 写文件的模式
# LOG_MAX_BYTES = 10 * 1024 * 1024  # 每个日志文件的最大字节数
# LOG_BACKUP_COUNT = 20  # 日志文件保留数量
# LOG_ENCODING = "utf8"  # 日志文件编码
# OTHERS_LOG_LEVAL = "ERROR"  # 第三方库的log等级
#
# # 切换工作路径为当前项目路径
# project_path = os.path.abspath(os.path.dirname(__file__))
# os.chdir(project_path)  # 切换工作路经
# sys.path.insert(0, project_path)
# print('当前工作路径为 ' + os.getcwd())
