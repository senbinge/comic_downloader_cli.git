import click

@click.group()
def cli():
    pass

@click.command()
def initdb():
    click.echo('Initialized the database')

@click.command()
def dropdb():
    click.echo('Dropped the database')

cli.add_command(initdb)
cli.add_command(dropdb)

#quick add
# @cli.command()
# def initdb():
#     click.echo('Initialized the database')

# @cli.command()
# def dropdb():
#     click.echo('Dropped the database')


@click.command()
@click.option('--count', default=1, help='number of greetings')
@click.argument('name')
def hello(count, name):
    for x in range(count):
        click.echo(f"Hello {name}!")
        
if __name__ == '__main__':
    cli()