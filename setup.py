from setuptools import setup,find_packages

setup(
    name='comic_downloader',
    version='0.1.0',
    py_modules=['main','book_download_spider','book_info_spider','book','chapter','setting'],
    install_requires=[
        'Click',
        'feapder'
    ],
    entry_points={
        'console_scripts':[
            'comic_downloader=main:cli'
        ]
    }
)