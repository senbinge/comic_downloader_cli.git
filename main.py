import click
from book_download_spider import BookDownloadSpider
import webbrowser

@click.group()
def cli():
    pass

@cli.command()
@click.option('--url',help='漫画网址',prompt='请输入漫画网址')
@click.option('--path',help='漫画保存地址',prompt='请输入漫画保存地址',default='./')
@click.option('--thread',help='下载线程数量',default=None)
def new(url,path,thread):
    '''
    新建漫画下载任务
    '''
    BookDownloadSpider.create_new_book(url,path,thread)

@cli.command()
def view():
    '''
    浏览漫画网站
    '''
    webbrowser.open('https://www.dongman.la/')

@cli.command()
@click.option('--path',prompt='book.dat文件地址')
def resume(path):
    '''
    继续未完成的下载任务
    '''
    BookDownloadSpider.continue_exist_book(path)

@cli.command()
@click.option('--path',prompt='漫画下载根目录')
def resume_all(path):
    '''
    继续目标目录下,所有未完成的下载任务
    '''
    BookDownloadSpider.continue_all_books(path)

if __name__=="__main__":
    cli()