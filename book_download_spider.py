



import feapder
import os.path as path
from feapder import Response
from book import Book
from book_info_spider import BookInfoSpider
import os
from feapder.utils.log import log
import time
import feapder.utils.tools as tools

class BookDownloadSpider(feapder.AirSpider):
    
    def __init__(self, book:Book,thread_count=None):
        super().__init__(thread_count=thread_count)
        self.book=book


    @staticmethod
    def create_new_book(book_url,save_path=None,thread_count=None):
        if not book_url:
            log.error('漫画url不能为空!')
            return
        if tools.is_valid_url(book_url)==False:
            log.error('漫画url非法!')
            return
        book=Book(book_url,save_path)
        # info_spider=BookInfoSpider(book)
        # info_spider.start()
        # while info_spider.all_thread_is_done()==False:
        #     time.sleep(1)

        BookDownloadSpider(book,thread_count).start()
    
    @staticmethod
    def continue_exist_book(book_path):
        if not book_path:
            log.error('文件不能为空!')
            return
        if path.exists(book_path)==False:
            log.error('文件不存在:'+str(book_path))
            return
        book=Book.load(book_path)
        BookDownloadSpider(book).start()
    
    @staticmethod
    def continue_all_books(folder):
        if path.exists(folder)==False:
            raise Exception('目录不存在:'+str(folder))
        # spiders=[]
        for root,_,files in os.walk(folder):
            if 'book.dat' in files:
                log.info('发现漫画下载记录:'+root)
                book=Book.load(path.join(root,'book.dat'))
                BookDownloadSpider(book).start()
                # spiders.append(BookDownloadSpider(book))
        # return spiders
    
    def start_requests(self):

        info_spider=BookInfoSpider(self.book)
        info_spider.start()
        while info_spider.all_thread_is_done()==False:
            time.sleep(1)
        
        if self.book.is_completed():
            log.info('漫画已经下载完成!')
            return
        log.info('漫画下载开始:'+self.book.title)
        for chapter in self.book.chapters.values():
            for image_id in chapter.get_task_list():
                image_name="%04d.jpg" % image_id
                url=chapter.url_pattern+image_name
                file_name=path.join(chapter.chapter_folder,image_name)
                yield feapder.Request(url,callback=self.save_image,file_name=file_name)


    

    def save_image(self,request,response:Response):
        file_name=request.file_name

        with open(file_name,'wb') as f:
            f.write(response.content)
        log.info('下载图片成功:'+file_name)


    def end_callback(self):
        if self.book.is_completed():
            log.info('漫画下载完成:'+self.book.title)
        else:
            log.warning('漫画下载未完成,请再次运行程序后下载!')
        return super().end_callback()



 

if __name__ == "__main__":
    # BookDownloadSpider.create_new_book('https://www.dongman.la/manhua/detail/11692/')
    # spiders=BookDownloadSpider.continue_all_books('./')
    # for spider in spiders:
    #     spider.start()
    BookDownloadSpider.continue_exist_book('./斗破苍穹 药老传奇')
