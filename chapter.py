from os import path
import os
class Chapter(object):
    def __init__(self,title:str,image_count:int,url_pattern:str,first_image_id:int) -> None:
        self.title=title
        self.image_count=image_count
        self.url_pattern=url_pattern
        self.first_image_id=first_image_id
        self.chapter_folder=''

    def is_completed(self):
        if path.exists(self.chapter_folder)==False:
            return False
        files=set(os.listdir(self.chapter_folder))
        for i in range(self.image_count):
            image_name='%04d'%(i+self.first_image_id)+'.jpg'
            if image_name not in files:
                return False
        return True
    
    def get_task_list(self):
        if path.exists(self.chapter_folder)==False:
            os.mkdir(self.chapter_folder)
            return [i+self.first_image_id for i in range(self.image_count)]
        tasks=[]
        files=set(os.listdir(self.chapter_folder))
        for i in range(self.image_count):
            image_name='%04d'%(i+self.first_image_id)+'.jpg'
            if image_name not in files:
                tasks.append(i+self.first_image_id)
        return tasks
    
    def to_dict(self):
        state= self.__dict__.copy()
        del state['chapter_folder']
        return state
    
    @classmethod
    def from_dict(cls,d:dict):
        return Chapter(
            title=d['title'],
            image_count=d['image_count'],
            url_pattern=d['url_pattern'],
            first_image_id=d['first_image_id']
        )
    

